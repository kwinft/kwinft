/*
SPDX-FileCopyrightText: 2015 Martin Gräßlin <mgraesslin@kde.org>

SPDX-License-Identifier: GPL-2.0-or-later
*/
#ifndef KWIN_TABBOX_LOGGING_H
#define KWIN_TABBOX_LOGGING_H

#include "kwin_export.h"

#include <QDebug>
#include <QLoggingCategory>
KWIN_EXPORT Q_DECLARE_LOGGING_CATEGORY(KWIN_TABBOX)

#endif
